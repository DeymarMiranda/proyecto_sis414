import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SkeletonDashboardComponent } from '@modules/dashboard/skeleton-dashboard/skeleton-dashboard.component';
import { SkeletonComponent } from './layout/skeleton/skeleton.component';


const routes: Routes = [
  {
    path:'',
    component:SkeletonComponent,
    children:[
      {
        path:'',
        loadChildren:()=>//importar primero el modulo para poder llamarlo
          import('@modules/home/home.module').then((m)=>m.HomeModule)
      },
      {
        path:'auth',
        loadChildren:()=>//importar primero el modulo para poder llamarlo
          import('@modules/auth/auth.module').then((m)=>m.AuthModule)

    }
    ]
  },
  {
    path:'dashboard',
    component:SkeletonDashboardComponent,
    children:[
      {
       path:'home',
       loadChildren:()=>//importar primero el modulo para poder llamarlo
          import('@modules/dashboard/dashboard.module').then((m)=>m.DashboardModule) 
      },
      {
        path:'publications',
        loadChildren:()=>//importar primero el modulo para poder llamarlo
           import('@modules/publications/publications.module').then((m)=>m.PublicationsModule) 
       }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
