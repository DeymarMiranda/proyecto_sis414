import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { CarComponent } from './car/car.component';
import { StatComponent } from './stat/stat.component';
import { OfertsComponent } from './oferts/oferts.component';
import { SharedModule } from '@shared/shared.module';
import { PublicationsRoutingModule } from './publications-routing.module';



@NgModule({
  declarations: [
    ProfileComponent,
    CarComponent,
    StatComponent,
    OfertsComponent
  ],
  imports: [
    SharedModule,
    PublicationsRoutingModule
  ]
})
export class PublicationsModule { }
