import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarComponent } from './car/car.component';
import { OfertsComponent } from './oferts/oferts.component';
import { ProfileComponent } from './profile/profile.component';
import { StatComponent } from './stat/stat.component';




const routes: Routes = [
    {
        path:'profile',
        component:ProfileComponent
    },
    {
        path:'stat',
        component:StatComponent
    },
    {
        path:'car',
        component:CarComponent
    },
    {
        path:'oferts',
        component:OfertsComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicationsRoutingModule { }

