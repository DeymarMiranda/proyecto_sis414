import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarDashboboardComponent } from './sidebar-dashboboard.component';

describe('SidebarDashboboardComponent', () => {
  let component: SidebarDashboboardComponent;
  let fixture: ComponentFixture<SidebarDashboboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarDashboboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarDashboboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
