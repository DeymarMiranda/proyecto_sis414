package main

import (
	"log"
	"net/http"
	"restapi/routes"
)

func main() {

	router := routes.Setup()
	server := http.ListenAndServe(":3300", router)
	log.Fatal(server)
}
