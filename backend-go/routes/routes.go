package routes

import (
	controller "restapi/Controllers"

	"github.com/gorilla/mux"
)

func Setup() *mux.Router {
	route := mux.NewRouter().StrictSlash(true)
	route.HandleFunc("/api/index", controller.Index)
	return route
}
